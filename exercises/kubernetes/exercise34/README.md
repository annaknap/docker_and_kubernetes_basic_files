# Exercise 34: Use revers proxy inside a POD

![multicontainerwebapp](images/multicontainerwebapp.png)

1. Change the directory
```bash
cd ~/workshop/exercises/kubernetes/exercise34/
```

2. Create a configmap
```bash
kubectl apply -f multi-container-web-configmap.yaml
```

3. Create an application
```bash
kubectl apply -f multi-container-web.yaml
```

4. Create a service
```bash
kubectl expose pod mc2 --type=NodePort --port=80
```

5. Check the configuration
```bash
kubectl describe service mc2
```

6. Check the application
```bash
while true; do sleep 0.1; curl $(minikube service mc2 --url -n test-multicontainer); done
```

7. Clean
```bash
kubens -
kubectl delete namespace test-multicontainer
```

[go to home](../../../README.md)

[go to next](../exercise35/README.md)
