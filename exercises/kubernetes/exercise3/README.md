# Exercise 3: Start dashboard

## Turn on dashboard 

```bash 
minikube dashboard
```
![kube-dashboard](images/kubernetes_dashboard.png)

## Look around in dashboard 
* Check resources 
* Check namespace

## Create applicaion 
* create application test-app from image nginx:1.19 (without service)

[go to home](../../../README.md)

[go to next](../exercise4/README.md)
