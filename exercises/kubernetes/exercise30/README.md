# Exercise 30: Hello_world_flask in dev and prod env



1. Change the directory
```bash
cd ~/workshop/exercises/kubernetes/exercise30/
```

![dir-structure](images/kustomize_hello_world_flask_white.png)


2. Create new namespace test-kustomize
```bash
kubectl create namespace test-kustomize
kubens test-kustomize
```

3. Create an application with production settings
```bash
kubectl apply -k overlays/production
```

4. Check pods
```bash
kubectl get all
```

5. Check the application by sending a request in loop
```bash
while true; do curl $( minikube service prod-http --url -n test-kustomize) ; sleep .3; done
```

6. In the second terminal show logs
```bash
stern prod-myapp
```

7. Start new application in dev env
```bash
kubectl apply -k overlays/development
```

8. Check pods
```bash 
kubectl get all
```

9. Check the application by sending a request in loop
```bash
while true; do curl $( minikube service dev-http --url -n test-kustomize) ; sleep .3; done
```

10. In the second terminal show logs
```bash
stern dev-myapp
```

11. Clean
```bahs
kubens -
kubectl delete namespace test-kustomize
```

[go to home](../../../README.md)

[go to next](../exercise31/README.md)
