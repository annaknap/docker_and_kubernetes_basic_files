# Exercise 23: Recreate a strategy

1. Change the directory
```bash
cd ~/workshop/exercises/kubernetes/exercise23
```

2. Create the first application
```bash
kubectl apply -f app-v1.yaml
```

3. Watch pods
```bash
watch kubectl get pod
```

4. In the second terminal check an application
```bash
while true; do sleep 0.1; curl $(minikube service my-app --url -n test-deploy); done
```

5. In the third terminal create the second application
```bash
kubectl apply -f app-v2.yaml
```

6. Clean
```bash
kubectl delete all -l app=my-app
```

[go to home](../../../README.md)

[go to next](../exercise24/README.md)
