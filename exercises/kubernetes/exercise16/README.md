# Exercise 16: Test quota in namespace

1. Create a namespace quota-example:
```bash
kubectl create namespace quota-example
kubens quota-example
````

2. Change the directory:
```
cd ~/workshop/exercises/kubernetes/exercise16/
```

3. Create a quota in namespace:
```bash
kubectl apply -f quota.yaml
kubectl describe quota
```

4. Create a pod:
```bash
kubectl apply -f busybox.yaml
```

5. Create limits:
```bash
kubectl apply -f limits.yaml
kubectl describe limits
```

6. Create one more time pod busybox:
```bash
kubectl apply -f busybox.yaml
```

7. Check a quota
```bash
kubectl describe quota
```

8. Create a deployment with application nginx with 5 replicas
* In the first terminal print events stream:
```bash
kubectl get events --watch
```

* In the second terminal create application:
```bash
kubectl create deployment nginx-test --image=nginx:1.15 --replicas=5
```

9. Clean env by deleting whole namespace:
```bash
kubens -
kubectl delete namespace quota-example
```

[go to home](../../../README.md)

[go to next](../exercise17/README.md)
