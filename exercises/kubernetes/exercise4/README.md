# Exercise 4: Start app from command line using cli (imperative approach)

## Create Pod

* Check options for run command 
```bash
kubectl run -h
```

* Create Pod using imange busybox with command sleep
```bash
kubectl run busybox-cli --image=busybox sleep 3600
```

* Check objects in current namespace
```bash
kubectl get all
```

* Check objects in all namespaces  
```bash
kubectl get all --all-namespaces|-A
```


[go to home](../../../README.md)

[go to next](../exercise5/README.md)
