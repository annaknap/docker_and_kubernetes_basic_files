# Exercise 11: Get information about namespace in POD in metadata

```bash 
kubectl explain pod.metadata
```

[go to home](../../../README.md)

[go to next](../exercise12/README.md)
