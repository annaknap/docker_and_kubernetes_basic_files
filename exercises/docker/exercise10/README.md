# Exercise 10: docker run --it

## In first terminal show all events 
```bash 
docker events 
```

## In second terminal run container with flag --it
```bash 
docker run -it --name test-it debian:latest /bin/bash
```

## Stop container by exit container and stop prints events in first terminal 
```bash 
Ctrl-C
```

[go to home](../../../README.md)

[go to next](../exercise11/README.md)
