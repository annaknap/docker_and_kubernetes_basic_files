# Exercise 24: Docker events

Docker server writes to event stream all information from the container cycle.
Only the last 1000 log events are returned.

## In fist termial print all events 
```bash 
docker events
```

## In the second terminal, start new container e.g.:
```bash 
docker run -d  --rm --name test-events busybox sleep 60
```

## Limit events by time

The --since and --until parameters can be Unix timestamps, date formatted timestamps, or Go duration strings (e.g. 10m, 1h30m) computed relative to the client machine’s time.

* Go duration string:  
```bash 
docker events --since '10m'
```

* formatted timestamp
```bash 
docker events --since '2021-05-24T12:00:00'
```

## Filtering

* filter by event
```bash 
docker events --filter 'event=stop' --since '10m'
```

* filter by container name 
```bash 
docker events --filter 'container=test-events' --since '10m'
```

* filter by type network
```bash 
docker events --filter 'type=network' --since '10m'
```

## Fromat the Output 

Using Go template:

```bash 
docker events --filter 'type=container' --format 'Type={{.Type}}  Status={{.Status}}  ID={{.ID}}' --since '10m'
```

or as json 

```bash 
docker events --format '{{json .}}'
```
[go to home](../../../README.md)

[go to next](../exercise25/README.md)
