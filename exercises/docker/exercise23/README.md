# Exercise 23: Docker stats

## Get stats from docker client:
```bash 
docker stats test-nginx
```

# Get stats directly from docker api:
```bash 
curl --unix-socket /var/run/docker.sock http:/v1.24/containers/<hash>/stats | head -n 1 | jq
```

[go to home](../../../README.md)

[go to next](../exercise24/README.md)
