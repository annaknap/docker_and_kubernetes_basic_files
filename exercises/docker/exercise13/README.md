# Exercies 13: Docker hostname

When container is created, Docker copy some file form file system.
One of them is /etc/hostname file.
The default value of it is container ID.

## Create container in interactive mode:
```bash 
docker run --rm=true -it --name=test-hostname ubuntu:latest /bin/bash
```
## Check hostname settings inside container: 
```bash 
mount
hostname -f
cat /etc/hostame
```
## Exit container: 
```bash 
CTRL-D
```

## Create new container with set hostname and check hostname settings 

```bash 
docker run --rm=true -it --name=test-hostname2  --hostname=mycontainer.example.com  ubuntu:latest /bin/bash
hostname -f
```
## Exit container: 
```bash 
CTRL-D
```
[go to home](../../../README.md)

[go to next](../exercise14/README.md)
