# Exercise 34: docker top

1.  Start container
```bash 
docker run -d --rm --name test_top debian:latest sh -c "while true; do echo 'tick'; sleep 1; done;"
```

2. Show all process
* using docker client
```bash
docker top test_top
docker top test_top auxfwww
```
* from host

```bash
ps auxfwww
ps axlfww
ps ejH
```
3. Clean
```bash
docker stop test_top
```

[go to home](../../../README.md)

[go to next](../exercise35/README.md)


